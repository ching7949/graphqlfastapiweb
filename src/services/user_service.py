
from src.auth import auth_handler
from src import config


def user_login(name):

    access_token = auth_handler.create_access_token(user_id=name, expiretime=config.auth_expiretime)

    return {'id': 1, 'name':name, 'token': access_token}
